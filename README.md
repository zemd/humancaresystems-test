# Test assignment

## Requirements

 - nvm (This project has been tested with node 13.2.0)
 - docker

## Preparation

 1. Run `nvm use`, this will ensure you are using correct node version
 2. Run `npm install`
 3. Run `docker-compose up`

### Configuration

All `bin/*.js` scripts are using `<project-root>/.env` file where environment variables are defined, like `MONGO_URL` and `MONGO_DATABASE`.
If you do not see it you can copy it from `.env.example`.

## Scripts

### Bootstrap

To check that everything is fine with mongodb connection and ensure you have correct collection in your database, run
`node bin/bootstrap.js`.

### Import data

To import data use command `node bin/import.js <filepath-to-file-with-data>`. The default file with data is located in
`<project-root>/fixtures/test1.txt`. By default, it is used by import script. If you want to pass it, you can use relative path to it,
for example, `node bin/import.js fixtures/test1.txt`.

### Validation

To validate data has been imported correctly, you can use `node bin/validate.js <filepath-to-file-with-data>`. Filepath
to file with initial data is required. The behavior according to the path is the same as for import script. For example,
`node bin/validate.js fixtures/test1.txt`


## Exploring database

If you don't have robomongo(or any other GUI mongo management tool) installed on your computer you can use `mongo-express`,
which is running along with mongo docker container. By default, it is accessible by url `http://localhost:8081`.

## Notes

There is second variant of task implementation, that is using `readline` module and `rxjs` library. It hasn't been fully tested,
due to lack of time, but was also sent to show another way of solving the problem. I think using `rxjs` much easier than using
streams.


## License

The code is released under the MIT license.
