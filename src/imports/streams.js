const through2 = require('through2');
const { runQuery } = require('../mongo');
const processFile = require('../processFile');

const insertManyToMongo = (inputStream) => {
  return through2.obj(
    function handleWrite(chunk, encoding, callback) {
      console.log(`About to run mongodb query for ${chunk.length} records`);
      // for very big files inputStream should be paused, since it reads much faster than mongo client can insert records
      // but `pause` operation is not instantaneous, so for the example file it might not be working as expected
      inputStream.pause();
      const emailSchedules = chunk.filter((item) => item.consent && item.email).map((item) => ({
        email: item.email,
        dates: [
          new Date(Date.UTC(2019, 4, 19, 0, 0, 0)),
          new Date(Date.UTC(2018, 9, 29, 0, 0, 0)),
          new Date(Date.UTC(2020, 7, 4, 0, 0, 0)),
          new Date(Date.UTC(2020, 1, 10, 0, 0, 0))
        ],
      }));

      Promise.all([
        runQuery((db) => db.collection('Patients').insertMany(chunk)),
        runQuery((db) => db.collection('Emails').insertMany(emailSchedules))
      ])
        .then(([, data]) => {
          callback(null, data.ops);
          inputStream.resume();
        })
        .catch((error) => {
          console.error('Got error during inserting values into mongo database', error);
          callback(error);
        });
    }
  );
};

module.exports = (file, batchCount = 10) => {
  return new Promise((resolve, reject) => {
    const inputStream = processFile(file, batchCount);

    inputStream
      .pipe(insertManyToMongo(inputStream))
      .on('data', (data) => {
        console.log(`${data.length} records have been inserted.`);
      })
      .on('finish', () => {
        console.log('The operation has been ended.');
        resolve();
      })
      .on('error', (error) => {
        console.error(error);
        reject(error);
      });
  });
};
