const { bufferCount, skip, map } = require('rxjs/operators');
const readline = require('../readline');
const { runQuery } = require('../mongo');

module.exports = (filepath, batchCount = 10) => {
  readline(filepath)
    .pipe(
      skip(1),
      map((line) => {
        const [
          pid,
          source,
          cardNumber,
          memberId,
          firstName,
          lastName,
          birthDate,
          address1,
          address2,
          city,
          state,
          zipcode,
          telephone,
          email,
          consent,
          mobile,
        ] = line.split('|');

        const [month, date, year] = birthDate.split('/');
        return {
          pid,
          source,
          cardNumber,
          memberId,
          firstName,
          lastName,
          birthDate: new Date(Date.UTC(year | 0, (month | 0) - 1, date | 0, 0, 0, 0)),
          address1,
          address2,
          city,
          state,
          zipcode,
          telephone,
          email,
          consent: consent === 'Y',
          mobile,
        };
      }),
      bufferCount(batchCount),
    )
    .subscribe((batchData) => {
      return runQuery((db) => db.collection('Patients').insertMany(batchData))
        .catch((error) => {
          console.error('Got error during inserting values into mongo database', error);
        });
    }, (error) => {
      console.log('error', error);
    })
};
