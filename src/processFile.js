const fs = require('fs');
const csv2 = require('csv2');
const through2 = require('through2');

const skipFirst = () => {
  let lineNumber = 0;
  return through2.obj(function handleWrite(chunk, encoding, callback) {
    lineNumber += 1;
    if (lineNumber > 1) {
      this.push(chunk);
    }
    callback();
  });
};

const transformCSV = () => {
  return through2.obj(function(chunk, encoding, callback) {
    const [
      pid,
      source,
      cardNumber,
      memberId,
      firstName,
      lastName,
      birthDate,
      address1,
      address2,
      city,
      state,
      zipcode,
      telephone,
      email,
      consent,
      mobile,
    ] = chunk;
    // console.log(`Transformed data for user(${firstName}, ${lastName})`);
    const [month, date, year] = birthDate.split('/');

    this.push({
      pid,
      source,
      cardNumber,
      memberId,
      firstName: firstName || null,
      lastName: lastName || null,
      birthDate: new Date(Date.UTC(year | 0, (month | 0) - 1, date | 0, 0, 0, 0)),
      address1: address1 || null,
      address2: address2 || null,
      city: city || null,
      state: state || null,
      zipcode: zipcode || null,
      telephone: telephone || null,
      email: email || null,
      consent: consent === 'Y',
      mobile: mobile || null,
    });
    callback();
  });
};

const bufferStream = function(count) {
  let buffer = [];
  return through2.obj(
    function handleWrite(chunk, encoding, callback) {
      buffer.push(chunk);
      if (buffer.length >= count) {
        this.push(buffer);
        buffer = [];
      }
      callback();
    },
    function handleFlush(callback) {
      if (buffer.length) {
        this.push(buffer);
        buffer = [];
      }
      callback();
    }
  );
};

/**
 * @param {string} file
 * @param {number} batchCount
 * @returns {Stream}
 */
module.exports = (file, batchCount = 10) => {
  return fs.createReadStream(file)
    .pipe(csv2({ separator: '|' }))
    .pipe(skipFirst())
    .pipe(transformCSV())
    .pipe(bufferStream(batchCount));
};
