const readline = require('readline');
const fs = require('fs');
const { Observable } = require('rxjs');

module.exports = (file) => {
  const rl = readline.createInterface({
    input: fs.createReadStream(file),
    crlfDelay: Infinity,
  });

  return new Observable((subscriber) => {
    rl.on('line', (line) => subscriber.next(line));
    rl.on('close', () => subscriber.complete());
  });
};
