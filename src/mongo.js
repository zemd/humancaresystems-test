const MongoClient = require('mongodb').MongoClient;
const genericPool = require('generic-pool');

const MONGO_URL = process.env.MONGO_URL;
const MONGO_DATABASE = process.env.MONGO_DATABASE;

const mongoPool = genericPool.createPool({
  async create() {
    try {
      console.log(`Using mongo URI string: ${MONGO_URL}`);
      const mongoClient = new MongoClient(MONGO_URL, { useNewUrlParser: true, useUnifiedTopology: true });
      await mongoClient.connect();
      return mongoClient;
    } catch (error) {
      console.error(error);
      process.exit(1);
    }
  },
  destroy(client) {
    client.close();
  }
}, { max: 2 });

exports.pool = mongoPool;

exports.runQuery = async (fn) => {
  return mongoPool.use((client) => Promise.resolve(fn(client.db(MONGO_DATABASE))));
};

// const exit = () => {
//   mongoPool.drain()
//     .then(function() {
//       return mongoPool.clear();
//     });
// };

// process.on('exit', () => exit()); // app is closing
// process.on('uncaughtException', () => exit()); // uncaught exceptions
// process.on('SIGINT', () => () => exit()); // ctrl+c event
