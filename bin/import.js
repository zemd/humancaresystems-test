require('dotenv').config();
// const importWithRxjs = require('./imports/rxjs');
const importWithStreams = require('../src/imports/streams');
const path = require('path');

const importFile = (file) => {
  const filepath = path.isAbsolute(file) ? file : path.join(process.cwd(), file);
  console.log(`About to import records from ${filepath}`);
  return importWithStreams(filepath);
};

console.log('--------------------------------------------------------------------------------');
console.log('process.env.MONGO_URL: ', process.env.MONGO_URL);
console.log('process.env.MONGO_DATABASE: ', process.env.MONGO_DATABASE);
console.log('--------------------------------------------------------------------------------');

importFile(process.argv[2] || path.join(__dirname, '../fixtures/test1.txt'))
  .then(() => process.exit())
  .catch(() => process.exit(1));
