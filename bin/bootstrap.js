require('dotenv').config();
const { pool } = require('../src/mongo');

const main = async () => {
  console.log('About to bootstrap database...');
  const client = await pool.acquire();
  console.log('Connection acquired');
  const db = client.db(process.env.MONGO_DATABASE);
  await db.createCollection('Patients', {
    validator: {
      '$jsonSchema': {
        bsonType: 'object',
        required: ['memberId'],
        properties: {
          pid: {
            bsonType: 'string'
          },
          source: {
            bsonType: 'string'
          },
          cardNumber: {
            bsonType: 'string'
          },
          memberId: {
            bsonType: 'string'
          },
          // firstName: {
          //   bsonType: 'string' // TODO: string or null
          // },
          lastName: {
            bsonType: 'string'
          },
          birthDate: {
            bsonType: 'date'
          },
          address1: {
            bsonType: 'string'
          },
          // address2: {
          //   bsonType: 'string' // TODO: string or null
          // },
          city: {
            bsonType: 'string'
          },
          state: {
            bsonType: 'string'
          },
          zipcode: {
            bsonType: 'string'
          },
          // telephone: {
          //   bsonType: 'string' // TODO: string or null
          // },
          // email: {
          //   bsonType: 'string' // TODO: string or null
          // },
          consent: {
            bsonType: 'bool'
          },
          mobile: {
            bsonType: 'string'
          }
        },
      },
    },
  });
  await db.createCollection('Emails', {});
  await db.collection('Patients').createIndex({ memberId: 1 });
  await db.collection('Patients').createIndex({ email: 1, consent: 1 });
  await db.collection('Emails').createIndex({ email: 1 });
  console.log('Collections `Patients` and `Emails` have been created.');
};

main()
  .then(() => process.exit())
  .catch((error) => console.error(error));
