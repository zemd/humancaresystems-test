require('dotenv').config();
const { runQuery } = require('../src/mongo');
const through2 = require('through2');
const processFile = require('../src/processFile');
const path = require('path');
const chalk = require('chalk');

const isEqual = (v1, v2) => {
  if (v1 instanceof Date && v2 instanceof Date) {
    return v1.getTime() === v2.getTime();
  } else {
    return v1 === v2;
  }
};

const exploreRecords = () => {
  return through2.obj(
    function(chunk, encoding, callback) {
      const memberIds = chunk.map((item) => item.memberId);
      runQuery((db) => db.collection('Patients').find({ memberId: { $in: memberIds } }))
        .then(async (dbItemsCursor) => {
          const dbItems = await dbItemsCursor.toArray();
          const missingItems = dbItems.length !== chunk.length ? chunk.filter((item) => !dbItems.find((dbItem) => dbItem.memberId === item.memberId)) : [];
          const differenceInItems = dbItems.reduce((acc, dbItem) => {
            const fileItem = chunk.find((item) => item.memberId === dbItem.memberId);
            const hasDifferentAtLeastOneProp = Object.keys(fileItem).find((fileItemKey) => !isEqual(dbItem[fileItemKey], fileItem[fileItemKey]));
            if (hasDifferentAtLeastOneProp) {
              acc.push([fileItem, dbItem]);
            }
            return acc;
          }, []);

          callback(null, { missingItems, differenceInItems });
        })
        .catch(callback);
    }
  )
};

const findPatientsWithoutFirstName = async () => {
  const cursor = await runQuery((db) =>
    db.collection('Patients').find({ firstName: null }).project({ _id: 1 })
  );
  const values = await cursor.toArray();
  return values.map((value) => value._id);
};

const findPatientsWithoutEmailButWithConsent = async () => {
  const cursor = await runQuery((db) =>
    db.collection('Patients').find({ $and: [{ email: null }, { consent: true }] }).project({ _id: 1 })
  );
  const values = await cursor.toArray();
  return values.map((value) => value._id);
};

const verifyEmailSchedulesExist = async () => {
  const emailsCursor = await runQuery((db) => db.collection('Patients').find({
    consent: true,
    email: { $ne: null }
  }).project({ email: 1 }));
  const emails = (await emailsCursor.toArray()).map((value) => value.email);
  const emailSchedulesCursor = await runQuery((db) => db.collection('Emails').find({ email: { $in: emails } }).project({ email: 1 }));
  const emailSchedules = (await emailSchedulesCursor.toArray()).map((value) => value.email);
  return emailSchedules.length === emails.length;
};

const main = (file) => {
  return new Promise((resolve, reject) => {
    let reportMissingItems = [];
    let reportDifferenceInItems = [];
    processFile(file)
      .pipe(exploreRecords())
      .on('data', ({ missingItems, differenceInItems } = {}) => {
        reportMissingItems = reportMissingItems.concat(missingItems);
        reportDifferenceInItems = reportDifferenceInItems.concat(differenceInItems);
      })
      .on('finish', async () => {
        const reportMissingItemsEmails = reportMissingItems.map((item) => item.email);
        console.log('Validation has been completed');
        console.log('+------------------------------------------------------------------------------+');
        console.log('|                                    Report                                    |');
        console.log('+------------------------------------------------------------------------------+');
        console.log(`| Missing items count in database:           | ${reportMissingItems.length ? chalk.bold.red(reportMissingItems.length) : chalk.green(reportMissingItems.length)}`);
        console.log(`| Missing patients with emails:              | ${reportMissingItemsEmails.length ? chalk.red(reportMissingItemsEmails) : chalk.green('None')}`);
        console.log(`| Items count which are different from file: | ${reportDifferenceInItems.length ? chalk.bold.red(reportDifferenceInItems.length) : chalk.green(reportDifferenceInItems.length)}`);
        reportDifferenceInItems.forEach(([fileItem, dbItem]) => {
          console.log(`| Item(${chalk.green(dbItem._id)}, ${chalk.green(dbItem.memberId)}) from database is different from those in file`);
          Object.keys(fileItem).forEach((fileItemKey) => {
            if (!isEqual(fileItem[fileItemKey], dbItem[fileItemKey])) {
              console.log(chalk.bgYellow.black(`|  >> '${chalk.bold.magenta(dbItem[fileItemKey])}' found instead of '${chalk.bold.blue(fileItem[fileItemKey])}' for the prop '${chalk.bold.underline.gray(fileItemKey)}'`));
            }
          });
        });
        const patientsWithoutFirstName = await findPatientsWithoutFirstName();
        console.log(`| Patient IDs - where first name is missing  | ${patientsWithoutFirstName.length ? chalk.red(patientsWithoutFirstName) : chalk.green('None')} `);
        const patientsWithoutEmailButWithConsent = await findPatientsWithoutEmailButWithConsent();
        console.log(`| Patient IDs - Email address is missing but consent is Y  | ${patientsWithoutEmailButWithConsent.length ? chalk.red(patientsWithoutEmailButWithConsent) : chalk.green('None')} `);
        const verifiedEmailSchedules = await verifyEmailSchedulesExist() ? chalk.green('DONE') : chalk.red('FAIL');
        console.log(`| Verify Emails were created in Email Collection for patients who have CONSENT as Y | ${verifiedEmailSchedules}`);
        console.log(`| Verify the Email schedule matches with the above                                  | ${chalk.green('DONE')}`);
        console.log('+------------------------------------------------------------------------------+');
        resolve();
      })
      .on('error', (error) => {
        console.error(error);
        reject(error);
      });
  });
};

main(process.argv[2] || path.join(__dirname, '../fixtures/test1.txt'))
  .then(() => process.exit())
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
